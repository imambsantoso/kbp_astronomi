#include<stdio.h>
#include<conio.h>
#include<iostream>
#include<math.h>
#include<string.h>
#include"astronomi.h"
using namespace std;

main () {
    int pilihan, nomor;
    cout << "====================================="<<endl;
    cout << "=============ASTRONOMI==============="<<endl;
    cout << "====================================="<<endl;
    cout <<endl;
    cout << "Pilihan: " << endl;
    cout << "1. Degree & Circle Shape "<<endl;
    cout << "2. Minute of Arc"<<endl;
    cout << "3. Second of Arc"<<endl;
    cout << "4. Small Angle Equation"<<endl;
    cout << "5. Periode Sinodis"<<endl;
    cout << "6. Hukum Kepler"<<endl;
    cout << "7. Momentum"<<endl;
    cout << "8. Law of Force"<<endl;
    cout << "9. Newton's Law of Gravity"<<endl;
    cout << "10. weight"<<endl;
    cout << "11. Kepler's 3rd law revised"<<endl;
    cout << "12. Orbital Speed"<<endl;
    cout << "13. Escape Velocity"<<endl;
    cout << "14. Energy Flux"<<endl;
    cout << "15. Kecepatan Cahaya (c)"<<endl;
    cout << "16. Frekuensi dan Panjang Gelombang"<<endl;
    cout << "17. Efek Dopler"<<endl;
    cout << "18. Energy of a Photon"<<endl;
    cout << "19. Light-Gathering Point"<<endl;
    cout << "20. Resolution"<<endl;
    cout << "21. temperature, mass, and speed"<<endl;
    cout << "22. Boltzmann's constant" <<endl;
    cout << "23. Ideal Gas Law"<<endl;
    cout << "24. Wien's Law"<<endl;
    cout << "25. Stefan-Boltzmann Law"<<endl;
    cout << "26. Energi Nuklir"<<endl;
    cout << "27. Tahun Cahaya"<<endl;
    cout << "28. parsec"<<endl;
    cout << "29. Parallax (Menghitung Jarak Bintang)"<<endl;
    cout << "30. Magnitude and Brightness"<<endl;
    cout << "31. Luminosity and Absolute Magnitude"<<endl;
    cout << "32. distance and magnitude"<<endl;
    cout << "33. The Mass for Each Star in a Binary Star System"<<endl;
    cout << "34. Luminosity and Mass"<<endl;
    cout << "35. Gravitational Energy"<<endl;
    cout << "36. Main Sequence Star's Lifetime"<<endl;
    cout << "37. Radius Lubang Hitam"<<endl;
    cout << "38. Hukum Kepler III"<<endl;
    cout << "39. Resolusi Matahari"<<endl;
    cout << "40. Massa Benda Langit (Planet, Bintang, dan Lainnya)"<<endl;
    cout << "41. Hubble's Law"<<endl;
    cout << "42. Eddington Luminosity"<<endl;
    cout << "43. hubble time"<<endl;
    cout << "44. Drake Equation"<<endl;
    cout << "45. Deskripsi Merkurius"<<endl;
    cout << "46. Deskripsi Venus"<<endl;
    cout << "47. Deskripsi Bumi"<<endl;
    cout << "48. Deskripsi Planet Mars"<<endl;
    cout << "49. Deskripsi Jupiter"<<endl;
    cout << "50. Deskripsi Saturnus"<<endl;
    cout << "51. Deskripsi Uranus"<<endl;
    cout << "52. Deskripsi Neptunus"<<endl;
    cout << "53. Deskripsi Pluto"<<endl;
    cout << "54. Deskripsi Matahari"<<endl;
    cout << "55. Deskripsi Bulan"<<endl;
    cout << "56. Finding the Mass of Procyon A & B"<<endl;

    cout << "Masukkan pilihan: ";
    cin >> pilihan;
    cout << "============================================================"<<endl;


//1.degree 1/360 of a circle
if (pilihan==1){
    long double o;
     cout << endl << "DEGREE" << endl;
     cout << "Masukkan data:" << endl;
     cout << "degree ="; cin >> o;
     cout << "circle shape = (o * 1)/360 of circle = " << degree << endl;}

//12.orbital speed : v = sqrroot(G(M(Sun))/d)
else if (pilihan==12){
    long double d;
      cout << endl << "ORBITAL SPEED" << endl;
      cout << "Masukkan data:" <<endl;
      cout << "Jaraknya= "; cin >> d;
      cout << "M(sun) = massa matahari (1.99*10^30)" << endl;
      cout << "G = gravitasi universal (6.67*10^-11)" << endl;
      cout << "v = sqrt(G*(M(Sun))/d) = " << orbital_speed << endl;}

//23.ideal gas : p=nkT
else if (pilihan==23){
    long double T, k, n;
     cout << endl << "IDEAL GAS" <<endl;
     cout << "Masukkan data:"<<endl;
     cout << "Suhu(Kelvin) = "; cin >> T;
     cout << "k = konstanta boltzmann (1.38*10^-23)" <<endl;
     cout << "Jumlah mol = "; cin >> n;
     cout << "p = n*k*T = " << ideal_gas <<endl;}

//34.luminosity and mass : L/L (Sun) = (M/M(Sun))^3.5
else if (pilihan==34){
    long double L,M;
     cout << endl << "LUMINOSITY AND MASS" <<endl;
     cout << "Masukkan data:"<<endl;
     cout << "L = "; cin >> L;
     cout << "M = "; cin >> M;
     cout << "M(sun) = massa matahari (1.99*10^30)" << endl;
     cout << "L(Sun) = luminositas matahari (3.84*10^26)" << endl;
     cout << "L = ((M/M(Sun))^3.5)*L (Sun) = " << luminosity1 <<endl;}

//45.Deskripsi Planet Merkurius
else if (pilihan==45){
     cout << endl << "DESKRIPSI PLANET MERKURIUS" << endl;
     cout << "Rotasi = " << rotasi_merkurius <<endl;
     cout << "Revolusi = " << revolusi_merkurius <<endl;
     cout << "Diameter = " << diameter_merkurius <<endl;
     cout << "Perbandingan Gravitasi terhadap Bumi = " << gravitasivsbumi_merkurius<<endl;
     cout << "Volume = " << volume_merkurius <<endl;
     cout << "Massa = " << massa_merkurius <<endl;
     cout << "Jarak terjauh terjauh terhadap matahari = " << terjauh_merkurius<<endl;
     cout << "Jarak terdekat terhadap mataharai = " << terdekat_merkurius<< endl;
     cout << "Jumlah Satelit = " << satelit_merkurius;}


//2. Minute of Arc : MoA=(degree/60)
else if (pilihan==2){
     long double derajat;
     cout << endl << "MINUTE OF ARC" << endl;
     cout << "Masukkan data:" << endl;
     cout << "degree = "; cin >> derajat;
     cout << "Minute = (degree/60)= " <<MoA<< endl;}

//13. Escape Velocity: v=sqrt((2*G*MSun/d))
else if (pilihan==13){
      long double d;
      cout << endl << "Escape Velocity" << endl;
      cout << "Masukkan data:" <<endl;
      cout << "G = gravitasi universal (6.67*10^-11)" << endl;
      cout << "M(sun) = massa matahari (1.99*10^30)" << endl;
      cout << "d = "; cin >> d;
      cout << "Escape Velocity= sqrt((2*G*MSun/d)) = " << escape_velocity << endl;}

//31. Wien's Law: Deltamax=(2.9*10^-3*m*K)/T
else if (pilihan==24){
     long double m,K,T;
     cout << endl << "WIEN's LAW" <<endl;
     cout << "Masukkan data:"<<endl;
     cout << "m = "; cin >> m;
     cout << "K = "; cin >> K;
     cout << "T = "; cin >> T;
     cout << "Wien's Law = (2.9*(10^-3)*m*K)/T)= " <<Deltamax<<endl;}

//35. Gravitational Energy: E=(G*M^2)/R
else if (pilihan==35){
     long double M,R;
     cout << endl << "GRAVITATIONAL ENERGY (E)" <<endl;
     cout << "Masukkan data:"<<endl;
     cout << "G = gravitasi universal (6.67*10^-11)" << endl;
     cout << "M = "; cin >> M;
     cout << "R = "; cin >> R;
     cout << "E = (G*M*M)/R = " << gravitational_energy;}

//53.Deskripsi Venus
else if (pilihan==46){
     cout << endl << "DESKRIPSI PLANET VENUS" << endl;
     cout << "Rotasi = " << rotasi_venus <<endl;
     cout << "Revolusi = " << revolusi_venus <<endl;
     cout << "Diameter = " << diameter_venus <<endl;
     cout << "Gravitasi venus = " << gravitasivsbumi_venus<<endl;
     cout << "Volume = " << volume_venus <<endl;
     cout << "Massa = " << massa_venus <<endl;
     cout << "Jarak terjauh terjauh terhadap matahari = " << terjauh_venus<<endl;
     cout << "Jarak terdekat terhadap mataharai = " << terdekat_venus<< endl;
     cout << "Jumlah Satelit = " << satelit_venus;}

//3. Second of Arc
else if (pilihan==3){
     char yes_no;
     long double minute;
     cout << endl << "SECOND OF ARC" << endl;
     cout << "Masukkan data:"<<endl;
     cout << "minute = "; cin >> minute;
     cout << "Second (minute/60) = " << second_of_arc << endl;
     cout << "Apakah anda ingin melihat penjelasannya? (y/t)";
     cin >> yes_no;
     while (yes_no!='t' && yes_no!='y')
        {cout << "M(sun) = massa matahari (1.99*10^30)" << endl;
            cout << "Pilihan anda tidak dikenali. Pilhlah y untuk ya atau t untuk tidak" << endl;
            cin >> yes_no;
        }
     if (yes_no=='y')
        {
            cout << "Dalam suatu lingkaran terdapat 360 derajat, " << endl
            << "dalam 1 derajat terdapat 60 menit, " << endl
            << "dan dalam 1 menit terdapat 60 sekon, " << endl
            << "sehingga second of arc adalah 1/60 menit atau 1/3600 derajat." << endl;
        }
    else if (yes_no=='t')
        {
        }
}

//14. Energy flux
else if (pilihan==14){
      long double E,d;
      cout << endl << "ENERGY FLUX" << endl;
      cout << "f = E/(4*pi*d^2)" <<endl
      <<"f= total energi yang melewati unit area per unit waktu (Watts m^-2)"<<endl
      <<"E= energi sumber (Watts)"<<endl
      <<"pi= konstanta pi (3.14519...)"<<endl
      <<"r= jarak ke sumber (m)"<<endl;
      cout << "Masukkan data:" <<endl;
      cout << "E = "; cin >> E;
      cout << "d = "; cin >> d;
      cout << "f = " << energy_flux << endl;
}

//25. Stefan-Boltzmann Law
else if (pilihan==25){
     long double T;
     cout << endl << "STEFAN BOLTZMANN LAW" <<endl;
     cout <<"E = stefan_boltzmann_constant*T^4" <<endl
     <<"E= total radiasi objek per meter kuadrat (Watts m^-2)"<<endl
     <<"stefan_boltzmann_constant= 5.67*10^-8 Watts m^-2 K^-4"<<endl
     <<"T= temperatur objek (K) "<<endl;
     cout << "Masukkan data:"<<endl;
     cout << "T = "; cin >> T;
     cout << "E = " << stefan_boltzmann_law <<endl;
}

//36. Main Sequence Star's Lifetime
else if (pilihan==36){
     long double M;
     cout << endl << "MAIN SEQUENCE STAR'S LIFETIME" <<endl;
     cout << "t = t(Sun)*((M/M(Sun))^(-2.5))" <<endl
     <<"t= lifetime bintang (tahun)"<<endl
     <<"t(Sun)= lifetime matahari (10^10 tahun)"<<endl
     <<"M= massa bintang (kg)"<<endl
     <<"M(sun)= massa matahari (1.99*10^30 kg)"<<endl;
     cout << "Masukkan data:"<<endl;
     cout << "M = "; cin >> M;
     cout << "t = " << main_sequence_star_lifetime;
}

//47.Deskripsi Bumi
else if (pilihan==47){
     cout << endl << "DESKRIPSI PLANET BUMI" << endl;
     cout << "Rotasi = " << rotasi_bumi <<endl;
     cout << "Revolusi = " << revolusi_bumi <<endl;
     cout << "Diameter = " << diameter_bumi <<endl;
     cout << "Gravitasi = " << gravitasi_bumi<<endl;
     cout << "Volume = " << volume_bumi <<endl;
     cout << "Massa = " << massa_bumi <<endl;
     cout << "Jarak terjauh terjauh terhadap matahari = " << aphelion_bumi<<endl;
     cout << "Jarak terdekat terhadap matahari = " << perihelion_bumi<< endl;
     cout << "Jumlah Satelit = " << satelit_bumi;
}

//4. Small Angle Equation
else if (pilihan==4){
    long double D,d;
     cout << endl << "SMALL ANGLE EQUATION" << endl;
     cout << "Masukkan data:" << endl;
     cout << "D = "; cin >>D;
     cout << "d = "; cin >>d;
     cout << "theta = 206265*(D/d) = " << small_angle_equation << endl;}

//15. Kecepatan Cahaya
else if (pilihan==15){
      cout << endl << "KECEPATAN CAHAYA (c)" << endl;
      cout << "c = " << kecepatan_cahaya << " m s^-2"<<endl;}

//26. Energi Nuklir
else if (pilihan==26){
    long double m;
     cout << endl << "ENERGI NUKLIR" <<endl;
     cout << "Masukkan data:"<<endl;
     cout << "m = "; cin >> m;
     cout << "c = kecepatan cahaya (3*10^8)" << endl;
     cout << "E = mc^2 = " << energi_nuklir <<endl;}

//37. Radius Lubang Hitam
else if (pilihan==37){
    long double M;
     cout << endl << "RADIUS LUBANG HITAM" <<endl;
     cout << "Masukkan data:"<<endl;
     cout << "M = "; cin >> M;
     cout << "G = gravitasi universal (6.67*10^-11)" << endl;
     cout << "R = (2*G*M)/(c^2) = " << radius_lubang_hitam;}

//48.Deskripsi Planet Mars
else if (pilihan==48){
     cout << endl << "DESKRIPSI PLANET MARS" << endl;
     cout << "Rotasi = " << rotasi_mars <<endl;
     cout << "Revolusi = " << revolusi_mars <<endl;
     cout << "Diameter = " << diameter_mars <<endl;
     cout << "Perbandingan Gravitasi terhadap Bumi = " << gravitasivsbumi_mars<<endl;
     cout << "Volume = " << volume_mars <<endl;
     cout << "Massa = " << massa_mars <<endl;
     cout << "Jarak terjauh terjauh terhadap matahari = " << terjauh_mars<<endl;
     cout << "Jarak terdekat terhadap mataharai = " << terdekat_mars<< endl;
     cout << "Jumlah Satelit = " << satelit_mars;}


//5. Periode Sinodis
else if (pilihan==5){
    long double P1,P2;
     cout << endl << "PERIODE SINODIS" <<endl;
     cout << "Masukkan data:" <<endl;
     cout << "P1 = "; cin >> P1;
     cout << "P2 = "; cin >> P2;
     cout << "S = 1/(1/P1-1/P2) = " << periode_sinodis <<endl;}

//16. Frekuensi dan Panjang Gelombang(c)
else if (pilihan==16){
    long double lambda,f;
    nomor = 0;
      cout << endl << "FREKUENSI DAN PANJANG GELOMBANG" <<endl;
	  cout << "Masukkan data:"<< endl;
	   cout << "Ketikan 1 untuk menghitung panjang gelombang" <<endl<< "Ketikan 2 untuk menghitung frekuensi : "; cin >> nomor;
        switch(nomor)
        {
            case 1:
            cout << "Masukkan data:" <<endl;
            cout << "f = "; cin >> f;
            cout << "lambda = c/f = " << lamda_on_frekuensi_dan_panjang_gelombang << endl;
            break;

            case 2:
            cout << "Masukkan data:" <<endl;
            cout << "lambda = "; cin >> lambda;
            cout << "f = c/lambda = " << lamda_on_frekuensi_dan_panjang_gelombang << endl;
            break;

            default:
			cout << "Pilihan tidak dikenali. Pilih 1 atau 2!\n";
			break;
        }
        }

//27. Tahun Cahaya : 9,46*10^11 km
else if (pilihan==27){
     cout << endl << "TAHUN CAHAYA" <<endl;
     cout << "Tahun Cahaya = :"<< tahun_cahaya <<endl;}

//38. Hukum Kepler III :
else if (pilihan==38){
     long double D,M;
     cout << endl << "HUKUM KEPLER III" <<endl;
     cout << "Masukkan data:"<< endl;
     cout << "D = "; cin >> D;
     cout << "M(sun) = massa matahari (1.99*10^30)" << endl;
     cout << "M = "; cin >> M;
     cout << "Hukum Kepler III = D^3/(MSun+M) = : " << hukum_kepler_III <<endl;
     }

//49.Deskripsi Jupiter
else if (pilihan==49){
     cout << endl << "DESKRIPSI PLANET JUPITER" << endl;
     cout << "Rotasi = " << rotasi_jupiter <<endl;
     cout << "Revolusi = " << revolusi_jupiter <<endl;
     cout << "Diameter = " << diameter_jupiter <<endl;
     cout << "Perbandingan Gravitasi terhadap Bumi = " << gravitasivsbumi_jupiter <<endl;
     cout << "Volume = " << volume_jupiter <<endl;
     cout << "Massa = " << massa_jupiter <<endl;
     cout << "Jarak terjauh terjauh terhadap matahari = " << terjauh_jupiter <<endl;
     cout << "Jarak terdekat terhadap mataharai = " << terdekat_jupiter <<endl;
     cout << "Jumlah Satelit = " << satelit_jupiter;}

//17. Efek Doppler : vr = cDL/LA
else if (pilihan==17){
     long double g,r;
      cout << endl << "Efek Doppler" <<endl;
      cout << "Masukkan data:" <<endl;
      cout << "Delta Lamda = "; cin >> g;
      cout << "Lamda Awal = "; cin >> r;
      cout << "Kecepatan radial = c * Delta Lamda/Lamda Awal  = " << kecepatan_radial << endl;}


//28. parsec :  3.09 x 10^16 m
else if (pilihan==28){
     cout << endl << "Parsec" <<endl;
     cout << "Parsec = "<< parsec <<endl;}

//39. Revolusi Matahari :  p = (2phid)/V
else if (pilihan==39){
    long double d,V;
     cout << endl << "Revolusi Matahari" <<endl;
     cout << "Masukkan data:"<< endl;
     cout << "pi = 3.14...." << endl;
     cout << "d = "; cin >> d;
     cout << "V = "; cin >> V;
     cout << "Revolusi Matahari = "<< revolusi_matahari <<endl;}

//50.Deskripsi Saturnus
else if (pilihan==50){
     cout << endl << "DESKRIPSI PLANET SATURNUS" << endl;
     cout << "Rotasi = " << rotasi_saturnus <<endl;
     cout << "Revolusi = " << revolusi_saturnus <<endl;
     cout << "Diameter = " << diameter_saturnus <<endl;
     cout << "Perbandingan Gravitasi terhadap Bumi = " << gravitasivsbumi_saturnus <<endl;
     cout << "Volume = " << volume_saturnus <<endl;
     cout << "Massa = " << massa_saturnus <<endl;
     cout << "Jarak terjauh terjauh terhadap matahari = " << terjauh_saturnus <<endl;
     cout << "Jarak terdekat terhadap mataharai = " << terdekat_saturnus <<endl;
     cout << "Jumlah Satelit = " << satelit_saturnus;}


//6. Hukum Kepler Ketiga asli : P1^2 = A1^3*A2^3/A2^3
else if (pilihan==6){
    long double f,a,b;
     cout << endl << "Hukum Kepler Ketiga Asli" << endl;
     cout << "Masukkan data:" << endl;
     cout << "P2 = "; cin >> f;
     cout << "A1 = "; cin >> a;
     cout << "A2 = "; cin >> b;
     cout << "P1^2 = A1^3 * P2^3/A2^3 = " << kepler_third_law << endl;}

//7. momentum : p = mv
else if (pilihan==7){
    long double m,v;
     cout << endl << "MOMENTUM (p = mv)" << endl;
     cout << endl << "deskripsi:" << endl<<endl
     <<"p = momentum (kg.m/s)"<<endl
     <<"m = massa (kg)"<<endl
     <<"v = kecepatan (m/s)"<<endl<<endl;
     cout << "Masukkan data :" << endl;
     cout << "m = "; cin >> m;
     cout << "v = "; cin >> v;
     cout << "p = mv = " << momentum << endl;
     }

//18. energy of a photon : E = hf = hc/lamda
else if (pilihan==18){
     nomor = 0;
     long double f, lamda;
     cout << "Ketikan 1 untuk rumus E = hf & Ketikan 2 untuk rumus E = hc/lamda : "; cin >> nomor;
        switch(nomor)
        {
            case 1:
            cout << endl << "ENERGY OF A PHOTON (E = hf)" << endl;
            cout << endl << "deskripsi :" << endl<<endl
            <<"E = Energi sebuah foton (Joule)" << endl
            <<"h = Konstanta Planck = 6,63 x 10^(-34) J.s"<<endl
            <<"f = Frekuensi foton cahaya (Hz)"<<endl<<endl;
            cout << "Masukkan data:" <<endl;
            cout << "f = "; cin >> f;
            cout << "E = hf = " << energy_of_a_photon_1 << endl;
            break;

            case 2:
            cout << endl << "ENERGY OF A PHOTON (E = hc/λ)" << endl;
            cout << endl << "deskripsi :" << endl<<endl
            <<"E = Energi sebuah foton (Joule)" << endl
            <<"h = Konstanta Planck = 6,63 x 10^(-34) J.s"<<endl
            <<"c = Kecepatan cahaya = 3 x 10^8 m/s"<<endl
            <<"lamda = panjang gelombang (m)"<<endl<<endl;
            cout << "Masukkan data:" <<endl;
            cout << "lamda = "; cin >> lamda;
            cout << "E = hc/lamda = " << energy_of_a_photon_2 << endl;
            break;

            default:
			cout << "Pilihan tidak dikenali. Pilihlah 1 atau 2!\n";
			break;

        }
}

//29. parallax : D = 1/p
else if (pilihan==29){
    long int p;
     cout << endl << "JARAK BINTANG (D = 1/p)" <<endl;
     cout << endl << "deskripsi :" << endl<<endl
     <<"D = Jarak Bintang (parsec)"<<endl
     <<"1 parsec sekitar 3,26 tahun cahaya atau sekitar 3.18 x 10^13 km" << endl<<endl
     <<"p = sudut parallax (arcsec)"<<endl<<endl;
     cout << "Masukkan data:"<<endl;
     cout << "p = "; cin >> p;
     cout << "D = 1/p = " << parallax <<endl;
     }

//40. Massa Bintang : M = ((v^2)d)/G
else if (pilihan==40){
    long double v,d;
     cout << endl << "MASSA BENDA LANGIT (PLANET, BINTANG, DLL) (M = ((v^2)d)/G)" <<endl<<endl;
     cout << endl << "deskripsi :" << endl<<endl
     <<"M = massa benda langit (kg)"<<endl
     <<"v = kecepatan orbit (km/s)"<<endl
     <<"d = jarak dari pusat gravitasi"<<endl
     <<"G = gravitasi universal = 6.67 x 10^-11 m^3/kg.s^2"<<endl<<endl;
     cout << "Masukkan data:"<<endl;
     cout << "v = "; cin >> v;
     cout << "d = "; cin >> d;
     cout << "M = ((v^2)d)/G = " << massa_benda_langit <<endl;
     }

//51. Deskripsi Uranus
else if (pilihan==51){
     cout << endl << "DESKRIPSI PLANET URANUS" << endl<<endl;
     cout << "Rotasi = " << rotasi_uranus <<endl;
     cout << "Revolusi = " << revolusi_uranus <<endl;
     cout << "Diameter = " << diameter_uranus <<endl;
     cout << "Perbandingan Gravitasi terhadap Bumi = " << gravitasi_uranus<<endl;
     cout << "Volume = " << volume_uranus <<endl;
     cout << "Massa = " << massa_uranus <<endl;
     cout << "Jarak terjauh terhadap matahari = " << aphelion_uranus<<endl;
     cout << "Jarak terdekat terhadap mataharai = " << perihelion_uranus<< endl;
     cout << "Jumlah Satelit = " << satelit_uranus;
}

//8. law of force (f=ma)
else if (pilihan==8){
     long double m,acc;
     cout << endl << "LAW OF FORCE" << endl;
     cout << "Masukkan data:" << endl;
     cout << "m = "; cin >> m;
     cout << "acc = "; cin >> acc;
     cout << "f = m*a = " << law_of_force << endl;}

//19. light-gathering point (A = phi*(r*r))
else if (pilihan==19){
    long double r;
      cout << endl << "LIGHT-GATHERING POINT" << endl;
      cout << "Masukkan data:" <<endl;
      cout << "r = "; cin >> r;
      cout << "pi = 3.14...." << endl;
      cout << "A = pi*r*r =  " << light_gathering_point << endl;}

//30. magnitude and brightness (b1/b2 =2.512^(m2-m1))
else if (pilihan==30){
    long double m1,m2;
     cout << endl << "MAGNITUDE AND BRIGHTNESS" <<endl;
     cout << "Masukkan data:"<<endl;
     cout << "m1 = "; cin >> m1;
     cout << "m2 = "; cin >> m2;
     cout << "b1/b2 = 2.512^(m1-m2) = " << b1bandingb2 <<endl;}

//41. hubble law (d = v/H) ; d = distance
else if (pilihan==41){
    long double v,H;
     cout << endl << "HUBBLE'S LAW" <<endl;
     cout << "Masukkan data:"<<endl;
     cout << "v = "; cin >> v;
     cout << "H = "; cin >> H;
     cout << "d = v/H = " << hubble_law;}

//52.Deskripsi Neptunus
else if (pilihan==52){
     cout << endl << "DESKRIPSI PLANET NEPTUNUS" << endl;
     cout << "Rotasi = " << rotasi_neptunus <<endl;
     cout << "Revolusi = " << revolusi_neptunus <<endl;
     cout << "Diameter = " << diameter_neptunus <<endl;
     cout << "Perbandingan Gravitasi terhadap Bumi = " << gravitasivsbumi_neptunus<<endl;
     cout << "Volume = " << volume_neptunus <<endl;
     cout << "Massa = " << massa_neptunus <<endl;
     cout << "Jarak terjauh terjauh terhadap matahari = " << terjauh_neptunus<<endl;
     cout << "Jarak terdekat terhadap matahari = " << terdekat_neptunus<< endl;
     cout << "Jumlah Satelit = " << satelit_neptunus;}

//9.newton's law of gravity : f = (GMm)/d^2
else if (pilihan==9){
    long double M,m,d;
     cout << endl << "NEWTON'S LAW OF GRAVITY" << endl;
     cout << "Masukkan data:" << endl;
     cout << "G = gravitasi universal (6.67*10^-11)" << endl;
     cout << "M = "; cin >> M;
     cout << "m = "; cin >> m;
     cout << "d = "; cin >> d;
     cout << "f = (GMm)/d^2 = " << newton_law_of_gravity << endl;}

//20.resolution = 250,000 (?/D)
else if (pilihan==20){
    long double xx,D;
      cout << endl << "RESOLUTION" << endl;
      cout << "Masukkan data:" <<endl;
      cout << "Panjang gelombang cahaya= "; cin >> xx;
      cout << "Diameter lensa aperture = "; cin >> D;
      cout << "Resolution = 250000*(Panjang gelombang/Diameter lensa) = " << resolusi << endl;}

//31.luminosity and absolute magnitude : M2 - M1
else if (pilihan==31){
    long double M1,L2,L1;
     cout << endl << "LUMINOSITY AND ABSOLUTE MAGNITUDE" <<endl;
     cout << "Masukkan data:"<<endl;
     cout << "M1 = "; cin >> M1;
     cout << "L1 = "; cin >> L1;
     cout << "L2 = "; cin >> L2;
     cout << "M2 = 2.5*log(L1/L2)+M1 = " << luminosity2<<endl;}

//42.eddington luminosity : L (edd) = 30,000 (M/M(Sun)) L (Sun);
else if (pilihan==42){
    long double M;
     cout << endl << "EDDINGTON LUMINOSITY (L(edd))" <<endl;
     cout << "Masukkan data:"<<endl;
     cout << "M = "; cin >> M;
     cout << "M(sun) = massa matahari (1.99*10^30)" << endl;
     cout << "L(Sun) = luminositas matahari (3.84*10^26)" << endl;
     cout << "L(edd) = 30000*(M/M(Sun))*L(Sun) = " << Ledd;}

//53.Deskripsi Pluto
else if (pilihan==53){
     cout << endl << "DESKRIPSI PLANET PLUTO" << endl;
     cout << "Rotasi = " << rotasi_pluto <<endl;
     cout << "Revolusi = " << revolusi_pluto <<endl;
     cout << "Diameter = " << diameter_pluto <<endl;
     cout << "Perbandingan Gravitasi terhadap Bumi = " << gravitasivsbumi_pluto<<endl;
     cout << "Volume = " << volume_pluto <<endl;
     cout << "Massa = " << massa_pluto <<endl;
     cout << "Jarak terjauh terjauh terhadap matahari = " << terjauh_pluto<<endl;
     cout << "Jarak terdekat terhadap mataharai = " << terdekat_pluto<< endl;
     cout << "Jumlah Satelit = " << satelit_pluto;}

//10. Weight w = [GM(Earth)m]/R^2
else if (pilihan==10){
    long double Me,m,R;
     cout << endl << "WEIGHT" << endl;
     cout << "Masukkan data:" << endl;
     cout << "G = gravitasi universal (6.67*10^-11)" << endl;
     cout << "M(earth) / Massa Bumi = "; cin >> Me;
     cout << "m (Massa Benda) = "; cin >> m;
     cout << "R (Radius Bumi) = "; cin >> R;
     cout << "w = [G*M(Earth)*m]/R^2 = " << weight << endl;}


//21. temperature, mass, and speed v=sqrt(8*k*T)/(phi*m)
else if (pilihan==21){
    long double T,mass;
      cout << endl << "TEMPERATURE, MASS, AND SPEED" << endl;
      cout << "Masukkan data:" <<endl;
      cout << "k = konstanta boltzmann (1.38*10^-23)" <<endl;
      cout << "T (temperature) = "; cin >> T;
      cout << "pi = 3.14...." << endl;
      cout << "m (mass) = "; cin >> mass;
      cout << "v=sqrt(8*k*T)/(pi*m) = " << speed_on_temperature_mass_and_speed << endl;}


//32. distance and magnitude m = M + 5 log (d/10)
else if (pilihan==32){
    long double M,d;
      cout << endl << "DISTANCE AND MAGNITUDE" << endl;
      cout << "Masukkan data:" <<endl;
      cout << "M (absolute magnitude) = "; cin >> M;
      cout << "d (distance) = "; cin >> d;
      cout << "m (apparent magnitude) = M + 5 log (d/10) = " << apparent_magnitude << endl;}


//43. hubble time t = d/v = d/Hd = 1/H
else if (pilihan==43){
    long double dd,vv;
      cout << endl << "HUBBLE TIME" << endl;
      cout << "Masukkan data:" <<endl;
      cout << "d (distance) = "; cin >> dd;
      cout << "v (velocity) = "; cin >> vv;
      cout << "t (hubble time) = (d/v) = " << hubble_time << endl;}


//54.Deskripsi Matahari
else if (pilihan==54){
     cout << endl << "DESKRIPSI MATAHARI" << endl;
     cout << "Diameter = " << diameter_matahari <<endl;
     cout << "Volume = " << volume_matahari <<endl;
     cout << "Massa = " << massa_matahari <<endl;
     cout << "Suhu = " << suhu_matahari <<endl;
     cout << "Gas yang terkandung = " << gasygterkandung <<endl;
     cout << "Jarak lidah api/ percikan api terjauh = " << lidahapi<<endl;
}

else if (pilihan==55){
    cout << endl << "DESKRIPSI BULAN"<< endl;
    cout << "Rotasi Bulan : " << rotasi_bulan <<endl;
    cout << "Diameter Bulan : " << diameter_bulan <<endl;
    cout << "Gravitasi Bulan : " << gravitasi_bulan <<endl;
    cout << "Volume Bulan : " << volume_bulan <<endl;
    cout << "Massa Bulan : " << massa_bulan <<endl;
    cout << "Periode Bulan : " << periode_bulan <<endl;
    cout << "Jari-Jari Kutub Bulan : " << jari_jari_kutub_bulan <<endl;
    cout << "Jari-Jari Khatulistiwa Bulan : " << jari_jari_khatulistiwa <<endl;
    cout << "Luas Permukaan Bulan : " << luas_permukaan_bulan <<endl;
    cout << "Kecepatan Rotasi Bulan : " << kecepatan_rotasi_bulan <<endl;
    cout << "Tekanan Atmosfir Bulan Siang Hari : " << tekanan_atmo_bulan <<endl;
    cout << "Tekanan Atmosfir Bulan Malam Hari : " << tekanan_atmo_bulan1 <<endl;
}

else if (pilihan==11){
     long double a,M1,M2;
     cout << endl << "KEPLER'S 3rd LAW REVISED" <<endl;
     cout << "Masukkan data: "<<endl;
     cout << "G = gravitasi universal (6.67*10^-11)" << endl;
     cout << "Pi = Konstanta"<<endl;
     cout << "a = "; cin >> a;
     cout << "M1 = "; cin >> M1;
     cout << "M2 = "; cin >> M2;
     cout << "Hasilnya = " <<kepler_3rd <<endl;

}

else if (pilihan==22){
     cout << endl << "BOLTZMANN'S CONSTANT" <<endl;
     cout << "Boltzmann's constant = " << boltzmanns_constant<<endl;
}

else if (pilihan==33){
     long double d,t;
     cout << endl << "THE MASS FOR EACH STAR IN A BINARY STAR SYSTEM" <<endl;
     cout << " Mass = d3/t2"<<endl;
     cout << "d (distance)= "; cin >> d;
     cout << "t (orbital period)= "; cin >> t;
     cout << "Mass = " << binary_star_system <<endl;
}

else if (pilihan==56){
     long double M,d,d1,d2;
     cout << "Pilih 1 untuk FINDING THE MASS OF PROCYON A &  2 untuk FINDING THE MASS OF PROCYON B : "; cin >> nomor;
        switch(nomor)
        {
            case 1:
            cout << endl << "FINDING THE MASS OF PROCYON A" <<endl;
            cout << " m1 = M*(d-d1)/d"<<endl;
            cout << "M (total mass) = "; cin >> M;
            cout << "d (distance between Procyon A & Procyon B) = "; cin >> d;
            cout << "d1 (Procyon A's distance from center of mass) = "; cin >> d1;
            cout << "m2= " << mass_procyon_A <<endl;
            break;

            case 2:
            cout << endl << "FINDING THE MASS OF PROCYON B" <<endl;
            cout << " m2 = M*(d-d2)/d"<<endl;
            cout << "M (total mass) = "; cin >> M;
            cout << "d (distance between Procyon A & Procyon B) = "; cin >> d;
            cout << "d2(Procyon B's distance from center of mass) = "; cin >> d2;
            cout << "m2 = " << mass_procyon_B <<endl;
            break;

            default:
			cout << "Pilihan tidak dikenali. Pilihlah 1 atau 2!\n";
			break;

        }
}

else if (pilihan==44){
     long double R,fp,ne,fl,fi,fc,L;
     cout << endl << "DRAKE EQUATION" <<endl;
     cout << "RUMUS => N = R*fp*ne*fl*fi*fc*L"<<endl;
     cout << "Masukkan data dibawah dengan benar"<<endl;
     cout<<endl;
     cout << "R = "; cin >> R;
     cout << "fp = "; cin >> fp;
     cout << "ne = "; cin >> ne;
     cout << "fl = "; cin >> fl;
     cout << "fi = "; cin >> fi;
     cout << "fc = "; cin >> fc;
     cout << "L = "; cin >> L;
     cout << "N = " << drake_equation <<endl;
}

getch();

}
