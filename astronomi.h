const double G=6.67*(0.00000000001);
const double c=3*(100000000);
const double konstanta_planck=6.63*(0.0000000000000000000000000000000001);
const double pi=3.14;
const double MSun=1990000; //nolnya dihilangin 25
const double tSun=1000000; //nolnya dihilangin 4
const double konstanta_stefan_boltzmann=0.0000000567;
const double LSun=3840000; //nolnya dihilangin 20
const double konstanta_boltzmann=0.0000000000000000000000138;

//------------------------------------------------------------------constant
#include<math.h>
//------------------------------------------------------------------header


//1. degree
#define degree o/360

//12. orbital speed : v = sqrroot(G(M(Sun))/d)
#define orbital_speed sqrt(G*MSun/d)

//23. ideal gas : p=nkT
#define ideal_gas n*k*T

//34. luminosity and mass : L/L (Sun) = (M/M(Sun))^3.5
#define luminosity1 (M/MSun)*(M/MSun)*(M/MSun)*sqrt(M/MSun)*LSun

//45.Deskripsi Merkurius
#define rotasi_merkurius "59 hari"
#define revolusi_merkurius "88 hari"
#define diameter_merkurius "4,879.4 km"
#define gravitasivsbumi_merkurius "0.054 x gravitasi bumi"
#define volume_merkurius "6.083 × 10^10 km^3"
#define massa_merkurius "3.3022 × 10^23 kg"
#define terjauh_merkurius "69,816,900 km"
#define terdekat_merkurius "46,001,200 km"
#define satelit_merkurius "tidak ada"

//-----------------------------------------------1

//2.minute of arc
#define MoA derajat/60
//13.escape velocity
#define escape_velocity sqrt((2*G*(MSun)/d))
//24.Wien's law
#define Deltamax (m*K*29/1000)/T
//35. Gravitational Energy
#define gravitational_energy (G*M*M)/R
//46.Deskripsi Venus
#define rotasi_venus "249 hari"
#define revolusi_venus "224.7 hari"
#define diameter_venus "12,104 juta km"
#define gravitasivsbumi_venus "8.87 m/s^2"
#define volume_venus "9.38 x 10^11 km^3"
#define massa_venus "4.8685 x 10^24 kg"
#define terjauh_venus "108,942,109 km"
#define terdekat_venus "107,476,259 km"
#define satelit_venus "Tidak punya"

//--------------------------------------------2

//3.second of arc
#define second_of_arc minute/60
//14.energy flux
#define energy_flux E/(4*pi*d*d)
//25.stefan-boltzmann law
#define stefan_boltzmann_law konstanta_stefan_boltzmann*T*T*T*T
//36.main sequence star's lifetime
#define main_sequence_star_lifetime tSun/((M/MSun)*(M/MSun)*sqrt(M/MSun))
//47.deskripsi bumi
#define rotasi_bumi "24 jam"
#define revolusi_bumi "365.25 hari"
#define diameter_bumi "12,742 km"
#define gravitasi_bumi "9.8 m/s^2"
#define volume_bumi "1,083,206,916,846 km^3"
#define massa_bumi "5.97 × 10^24 kg"
#define aphelion_bumi "152,098,232 km"
#define perihelion_bumi "147,098,290 km"
#define satelit_bumi "1"

//---------------------------------------------------------------3

//4. Small Angle Equation
#define small_angle_equation D/d*206265
//15. Kecepatan Cahaya
#define kecepatan_cahaya c
//26. Energi Nuklir
#define energi_nuklir m*c*c
//37. Radius Lubang Hitam
#define radius_lubang_hitam (2*G*M)/(c*c)
//48.Deskripsi Mars
#define rotasi_mars "24 jam 37 menit"
#define revolusi_mars "687 hari"
#define diameter_mars "6,792 km"
#define gravitasivsbumi_mars "1/3 x gravitasi bumi"
#define volume_mars "1.6318 x 10^11 km³"
#define massa_mars "6.4185 x 10^23 kg"
#define terjauh_mars "249,209,300 km"
#define terdekat_mars "206,669,000  km"
#define satelit_mars "2"

//-------------------------------------------------4

//5. periode sinodis S = 1/(1/P1-1/P2)
#define periode_sinodis 1/P1-1/P2

//16.frekuensi dan panjang gelombang
#define lamda_on_frekuensi_dan_panjang_gelombang c/f
#define frekuensi_on_frekuensi_dan_panjang_gelombang c/lambda

//27. tahun cahaya
#define tahun_cahaya "9.46*10^11 km"

//38. hukum kepler III (newton)
#define hukum_kepler_III D*D*D/(MSun+M)

//49. deskripsi Jupiter
#define rotasi_jupiter "10 jam"
#define revolusi_jupiter "11.86 tahun"
#define diameter_jupiter "143,000 km"
#define gravitasivsbumi_jupiter "2.5 x gravitasi bumi"
#define volume_jupiter "1.4313 × 10^15 km³"
#define massa_jupiter "1.8986 × 10^27 kg"
#define terjauh_jupiter "817 juta km"
#define terdekat_jupiter "741 juta km"
#define satelit_jupiter "66 satelit"

//-------------------------------------------------5

//17. Efek Doppler
#define kecepatan_radial (c*g)/r
//28. parsec :  3.09 x 10^16 m
#define parsec "3.09 x 10^16 m"
//39. Revolusi Matahari
#define revolusi_matahari (d*pi*2)/V
//50. deskripsi Saturnus
#define rotasi_saturnus "10 jam 14 menit waktu Bumi"
#define revolusi_saturnus "30 tahun "
#define diameter_saturnus "120,536 km (74,867 mil)"
#define gravitasivsbumi_saturnus "1.16 x gravitasi bumi"
#define volume_saturnus "8.2713 x 1014 km³"
#define massa_saturnus "5.6846 x 1026 kg"
#define terjauh_saturnus "1.51 milyar km"
#define terdekat_saturnus "1.35 milyar km"
#define satelit_saturnus "59 satelit"
//6. Hukum Kepler Ketiga
#define kepler_third_law a*a*a*f*f/b*b*b

//-------------------------------------------------6

//7. momentum
#define momentum m*v


//18. energy of a photon : E = hf = hc/lamda
#define energy_of_a_photon_1 konstanta_planck*f
#define energy_of_a_photon_2 (konstanta_planck*c)/lamda

//29. parallax
#define parallax 1/p

//40. Massa Bintang
#define massa_benda_langit ((v*v)*d)/G


//51. Deskripsi Uranus
#define rotasi_uranus "17.25 jam"
#define revolusi_uranus "84.02 tahun"
#define diameter_uranus "51.118 km"
#define gravitasi_uranus "1.17 x gravitasi bumi"
#define volume_uranus "6.833 x 10^13 km^3"
#define massa_uranus "14,536 x massa bumi"
#define aphelion_uranus "3.00 milyar km"
#define perihelion_uranus "2.75 milyar km"
#define satelit_uranus "27"


//-------------------------------------------------7

//8. law of force (f=ma)
#define law_of_force m*acc

//19. light-gathering point (A = (lambda*(D^2))/4 = phi*(r^2))
#define light_gathering_point pi*r*r

//30. magnitude and brightness (b1/b2 =2.512^(m2-m1))
#define b1bandingb2 pow(2.512,(m2-m1))


//41. hubble law (d = v/H) ; d = distance
#define hubble_law v/H

//53.Deskripsi Pluto
#define rotasi_neptunus "16.1 jam"
#define revolusi_neptunus "164.8 tahun"
#define diameter_neptunus "49,530 km"
#define gravitasivsbumi_neptunus "57.74 x gravitasi bumi"
#define volume_neptunus "6.254 x 10^13 km³"
#define massa_neptunus "1.0243 x 10^26 kg"
#define terjauh_neptunus "4,553,946,490 km"
#define terdekat_neptunus "4,452,940,833 km"
#define satelit_neptunus "14"

//-------------------------------------------------8

//9.newton's law of gravity
#define newton_law_of_gravity G*M*m/(d*d)
//20.resolution = 250,000
#define resolusi 250000*xx/D
//31.luminosity and absolute magnitude
#define luminosity2 2.5*(log(L1/L2))+M1
//42.eddington luminosity
#define Ledd 30000*(M/MSun)*LSun
//53.Deskripsi Pluto
#define rotasi_pluto "6.39 hari"
#define revolusi_pluto "48.4 tahun"
#define diameter_pluto "2,486 km"
#define gravitasivsbumi_pluto "0.059 x gravitasi bumi"
#define volume_pluto "6.39 x 10^9 km³"
#define massa_pluto "(1.305 +_ 0.007) x 10^22 kg"
#define terjauh_pluto "7,38 milyar km"
#define terdekat_pluto "4,44 milyar km"
#define satelit_pluto "5"

//-------------------------------------------------9

//10.weight
#define weight G*Me*m/R*R

//21.temperature, mass, and speed
#define speed_on_temperature_mass_and_speed sqrt(8*konstanta_boltzmann*T)/(pi*mass)

//32.distance and magnitude
#define apparent_magnitude M+(5*log10(d)/10)

//43.hubble time
#define hubble_time (dd/vv)

//54.Deskripsi Matahari
#define diameter_matahari "1.392684*10^6 km"
#define volume_matahari "1.412*10^18  km³"
#define massa_matahari "1.9891*10^30 kg"
#define suhu_matahari "Permukaan 5,700 derajat celcius"
#define gasygterkandung "hidrogen,helium"
#define lidahapi "350,000 km"

//------------------------------------------------10
//55 Bulan
#define rotasi_bulan "4,627 meter per second"
#define diameter_bulan "4.378.000 m"
#define gravitasi_bulan "1,622 m/s^2"
#define volume_bulan "2,1958×10^10 km3"
#define massa_bulan "7.3477 × 10^22 kg"
#define periode_bulan "27d 7h 43,1 min"
#define jari_jari_kutub_bulan "1.735,97 km"
#define jari_jari_khatulistiwa "1.738,14 km"
#define luas_permukaan_bulan "3,793×107 km^2"
#define kecepatan_rotasi_bulan "4,627 m/s"
#define tekanan_atmo_bulan "10^−7 Pa (Siang)"
#define tekanan_atmo_bulan1 "10^−10 Pa (Malam)"

//Kepler's 3rd law revised p^2 = (4p^2a^3)/G(m + M)
#define kepler_3rd 4*pi*4*pi*a*a*a/G*M1+G*M2

//boltzmann's constant 1.38 x 10^-23 kg m^2/K s^2
#define boltzmanns_constant "1.38 x 10^-23 kg m^2/K s^2"

//the Mass for Each Star in a Binary Star System
#define binary_star_system d*d*d/t*t

//Finding the Mass of Procyon A
#define mass_procyon_A M*(d-d1)/d

//Formula for Finding the Mass of Procyon B
#define mass_procyon_B M*(d-d2)/d

//drake equation N = R*fp*ne*fl*fi*fc*L
#define drake_equation R*fp*ne*fl*fi*fc*L


//------------------------------------------------11


